package com.example.exercise_project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.exercise_project.databinding.FragmentDetailBodyWeightBinding
import com.example.exercise_project.databinding.FragmentHomeBinding


class DetailBodyWeightFragment : Fragment() {
    private var _binding: FragmentDetailBodyWeightBinding? = null
    private val binding get() = _binding!!
    private val navigationArgs: DetailBodyWeightFragmentArgs by navArgs()
    var itemId = 0
    var selectId = 0
    private val normal = com.example.exercise_project.data.BodyData().getLevelNormal()
    private val hard = com.example.exercise_project.data.BodyData().getLevelHard()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBodyWeightBinding.inflate(inflater,container,false)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemId = navigationArgs.idPage
        selectId = navigationArgs.selectLevel
        if(selectId == 1){
            binding.imgDetail.setImageResource(normal[itemId].imageId)
            binding.rptDetail.text = normal[itemId].detail
            binding.rptName.text = normal[itemId].name
        }else if(selectId == 2){
            binding.imgDetail.setImageResource(hard[itemId].imageId)
            binding.rptDetail.text = hard[itemId].detail
            binding.rptName.text = hard[itemId].name
        }
        binding.button.setOnClickListener{
            val action = DetailBodyWeightFragmentDirections.actionDetailBodyWeightFragmentToListBodyWeight(level = selectId,itemId=itemId)
            findNavController().navigate(action)
        }





    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}