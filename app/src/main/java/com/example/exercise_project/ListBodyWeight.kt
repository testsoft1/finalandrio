package com.example.exercise_project

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.exercise_project.data.BodyData
import com.example.exercise_project.databinding.FragmentListBodyWeightBinding

import com.example.exercise_project.model.Body


class ListBodyWeight : Fragment() {
    private var _binding: FragmentListBodyWeightBinding? = null
    private val binding get() = _binding!!
    private val navigationArgs: ListBodyWeightArgs by navArgs()
    lateinit var dataNormal: List<Body>
    val dataHard = BodyData().getLevelHard()
    private val normal = com.example.exercise_project.data.BodyData().getLevelNormal()
    private val hard = com.example.exercise_project.data.BodyData().getLevelHard()
    private lateinit var bodyNormal: Body
    private lateinit var bodyHard: Body
    var itemId = 0
    var selectLevel = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBodyWeightBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectLevel = navigationArgs.level
        itemId = navigationArgs.itemId

        findItem()
        init()
        nextScreen()
        clickImage()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun nextExerciseOrComplete() {
        if (itemId == normal.size && selectLevel == 1) {
            val action = ListBodyWeightDirections.actionListBodyWeightToCompleteExerciseFragment()
            findNavController().navigate(action)

        } else if (itemId == hard.size && selectLevel == 2) {
            val action = ListBodyWeightDirections.actionListBodyWeightToCompleteExerciseFragment()
            findNavController().navigate(action)
        } else {
            if (selectLevel == 1) {
                setTextNormalExercise()
            } else if (selectLevel == 2) {
                setTextHardExercise()
            }
        }

    }

    private fun setTextNormalExercise() {
        binding.apply {
            step.text = normal[itemId].id.toString()
            detailName.text = normal[itemId].name
            detailTimes.text = normal[itemId].times
            exerciseImage.setImageResource(normal[itemId].imageId)
        }
    }

    private fun setTextHardExercise() {
        binding.apply {
            step.text = hard[itemId].id.toString()
            detailName.text = hard[itemId].name
            detailTimes.text = hard[itemId].times
            exerciseImage.setImageResource(hard[itemId].imageId)
        }
    }

    private fun findItem() {
        normal.forEach {
            bodyNormal = it
            Log.d("selectLevel", "$normal ")
        }

        hard.forEach {
            bodyHard = it
        }
    }

    private fun init() {
        if (selectLevel == 1) {
            setTextNormalExercise()
        } else if (selectLevel == 2) {
            setTextHardExercise()
        }

    }

    private fun nextScreen() {
        binding.btnNext.setOnClickListener {
            itemId++
            clickImage()
            nextExerciseOrComplete()
        }
    }

    private fun clickImage() {
        binding.exerciseImage.setOnClickListener {
            val action = ListBodyWeightDirections.actionListBodyWeightToDetailBodyWeightFragment(
                idPage = itemId,
                selectLevel = selectLevel
            )
            findNavController().navigate(action)
        }
    }


}