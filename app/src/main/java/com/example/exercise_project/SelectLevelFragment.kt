package com.example.exercise_project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.exercise_project.data.BodyData
import com.example.exercise_project.databinding.FragmentHomeBinding
import com.example.exercise_project.databinding.FragmentSelectLevelBinding

class SelectLevelFragment : Fragment() {
    private var _binding: FragmentSelectLevelBinding? = null
    private val binding get() = _binding!!
    var index = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSelectLevelBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.LevelNormalBtn.setOnClickListener{
            val action = SelectLevelFragmentDirections.actionSelectLevelFragmentToListBodyWeight(level = 1, itemId = 0)
            findNavController().navigate(action)
        }
        binding.levelHardBtn.setOnClickListener{
            index++
            val action = SelectLevelFragmentDirections.actionSelectLevelFragmentToListBodyWeight(level = 2, itemId = 0)
            findNavController().navigate(action)
        }

    }


}