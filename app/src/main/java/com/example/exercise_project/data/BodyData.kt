package com.example.exercise_project.data

import com.example.exercise_project.R
import com.example.exercise_project.model.Body

class BodyData {
    fun getLevelNormal(): List<Body> {
        return listOf(
            Body(
                id = 1,
                name = "SQUAD",
                times = "X16",
                imageId = R.drawable.exer,
                detail = "ก"
            ),
            Body(
                id = 2,
                name = "SIT UP",
                times = "X10",
                imageId = R.drawable.exer,
                detail = "ก"
            ),
            Body(
                id = 3,
                name = "SIT UP2",
                times = "X10",
                imageId = R.drawable.exer,
                detail = "กasdasdsad"
            ),

        )
    }
    fun getLevelHard(): List<Body> {
        return listOf(
            Body(
                id = 1,
                name = "SQUAD",
                times = "X20",
                imageId = R.drawable.exer,
                detail = "ก"
            ),
            Body(
                id = 2,
                name = "SIT UP1",
                times = "X30",
                imageId = R.drawable.exer,
                detail = "กasdsad"
            ),

        )
    }
}