package com.example.exercise_project.model

import androidx.annotation.DrawableRes

data class Body(
    val id: Int,
    val name: String,
    val times:String,
    @DrawableRes val imageId: Int,
    val detail: String
)
